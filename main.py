import asyncio
import mybot
import logging
from aiogram import Bot, Dispatcher, types
from aiogram.types import ParseMode
from aiogram.utils import executor
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
import uuid
from openai import OpenAI
import down



API_TOKEN = mybot.API_TELEGRAM

client = OpenAI(
    api_key="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",
)

logging.basicConfig(level=logging.INFO)

bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)

from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
import uuid  # Модуль для генерации уникальных идентификаторов

async def create_inline_keyboard(*buttons_text):
    keyboard = InlineKeyboardMarkup()
    for button_text in buttons_text:
        keyboard.add(InlineKeyboardButton(text=button_text, callback_data=str(uuid.uuid4())))
    return keyboard

@dp.message_handler(commands=['start'])
async def send_welcome(message: types.Message):

    try:
        start_mess, stream = mybot.start()
        if stream:
            message = await message.answer(start_mess[:1])
            for i in range(len(start_mess[:2]), len(start_mess), 10):
                await asyncio.sleep(0.05)
                partial_text = start_mess[:i + 10]
                await bot.edit_message_text(chat_id=message.chat.id, message_id=message.message_id, text=partial_text,
                                                parse_mode='HTML')
        else:
            message = await message.answer(start_mess)

    except ValueError:
        start_mess = mybot.start()
        message = await message.answer(start_mess)

    except TypeError:
        print("ОШИБКА: В функции start вы забыли указать сохранить с помощью return параметр text")


    try:
        keyboard = await create_inline_keyboard(*mybot.start_buttons())
        await bot.edit_message_reply_markup(chat_id=message.chat.id, message_id=message.message_id, reply_markup=keyboard)
    except:
        pass



    
def chatgpt(model,text):
  chat_completion = client.chat.completions.create(
      messages=[
          {
              "role": "system",
              "content": 'Будь помощником!'},
          {
                  # Выбираем роль - пользователь т.е. того кто будет спрашивать
              "role": "user",
                  # Здесь мы выводим текст который ввёл пользователь
              "content": text,
          }
      ],
          # Модели бывают разные, мы будем использовать самую популярную и быструю
      model="gpt-3.5-turbo",
      )
      # Получаем ответ от ChatGPT
  bot_reply = chat_completion.choices[0].message.content
  return bot_reply
        
  

@dp.message_handler(commands=['mode'])
async def mode_welcome(message: types.Message):

  try:
    mode_mess, stream = mybot.mode()
    if stream:
        message = await message.answer(mode_mess[:1])
        for i in range(len(mode_mess[:2]), len(mode_mess), 10):
            await asyncio.sleep(0.05)
            partial_text = mode_mess[:i + 10]
            await bot.edit_message_text(chat_id=message.chat.id, message_id=message.message_id, text=partial_text,
                                            parse_mode='HTML')
    else:
        message = await message.answer(mode_mess)

  except ValueError:
    mode_mess = mybot.mode()
    message = await message.answer(mode_mess)

  except TypeError:
    print("ОШИБКА: В функции start вы забыли указать сохранить с помощью return параметр text")


  try:
    keyboard = await create_inline_keyboard(*mybot.mode_buttons())
    await bot.edit_message_reply_markup(chat_id=message.chat.id, message_id=message.message_id, reply_markup=keyboard)
  except:
    pass




@dp.message_handler()
async def echo(message: types.Message):
  x = 0
  input_usr_message = message.text

  try:
    msg, stream = mybot.answer_bot(input_usr_message) 
    x = 1
    if msg != None and stream:
      message = await message.answer(msg[:1])
      for i in range(len(msg[:2]), len(msg), 10):
        await asyncio.sleep(0.05)
        partial_text = msg[:i + 10]
        await bot.edit_message_text(chat_id=message.chat.id, message_id=message.message_id, text=partial_text,
                                              parse_mode='HTML')

    else: 
      msg = mybot.answer_bot(input_usr_message) 
      
  except:
    pass


  if x == 0:
    try:
      msg = mybot.answer_bot(input_usr_message) 
      if msg != None:
        await message.answer(msg)
    except:
      pass

    
if __name__ == '__main__':
    from aiogram import executor
    down.custom_speed_loading(duration_seconds=5)
    executor.start_polling(dp, skip_updates=True)
